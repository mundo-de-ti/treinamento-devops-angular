# Front End - Projeto Web padrão
<br><br>
# Pré-requisitos

### Instale o Node JS

[Node JS](https://nodejs.org).

### Instale o Angular CLI

Após instalar o Node JS, instale o Angular CLI, executando o comando `npm install -g @angular/cli`

<br>
# Sistema Padrao


### Instalar Dependências

Para instalar as dependências, execute `npm install --loglevel=error`

### Development server

Execute `ng serve` para subir o ambiente de desenvolvimento. Navegue usando o endereço `http://localhost:4200/`. O app será automaticamente atualizado se você fizer qualquer alteração no código.

### Build

Execute `ng build` para gerar a build do projeto. A build será gerada na pasta `dist/` . Use `-prod` para gerar uma build em produção.

### Rodando Testes Unitários

Execute `ng test` para rodar os testes unitários, usando [Karma](https://karma-runner.github.io).

### Rodando testes end-to-end

Execute `ng e2e` para rodar os testes end-to-end, usando [Protractor](http://www.protractortest.org/).

### Mais informações

Para maiores informações execute no prompt o comando Angular CLI `ng help`.
