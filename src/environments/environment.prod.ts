export const environment = {
  production: true,
  versaoSistema: '#{versao_sistema_web}#',
  urlService: '#{url_api}#',
  user: '#{user_api}#',
  apipass: '#{pass_api}#'
};
