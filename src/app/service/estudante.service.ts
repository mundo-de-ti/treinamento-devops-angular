import { Http } from '@angular/http';
import { RequestOptions } from '@angular/http';
import { Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Estudante } from './../model/estudante';

@Injectable()
export class EstudanteService {

  private URL_AUTOR = environment.urlService + '/estudantes';
  private username = environment.user;
  private password = environment.apipass;

  constructor(private http: Http) { }


  getOptions(): RequestOptions {

    let headers = new Headers();  
    headers.append("Authorization", "Basic " + btoa(this.username + ":" + this.password));
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });

    return options;
  }

  save(estudante: Estudante): Observable<Estudante> {

    return this.http.post(this.URL_AUTOR + '/', estudante, this.getOptions())
      .map(res => res.text())
      .catch((error: any) => Observable.throw(error.json() || 'Server error'));
  }

  getEstudantees(): Observable<Estudante[]> {

    return this.http.get(this.URL_AUTOR + '/lista/', this.getOptions())
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json() || 'Server error'));
  }

  getEstudante(id): Observable<Estudante> {

    return this.http.get(this.URL_AUTOR + '/' + id, this.getOptions())
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json() || 'Server error'));
  }

  excluirEstudante(id): Observable<string> {

    return this.http.delete(this.URL_AUTOR + '/' + id, this.getOptions())
      .map(res => res.text())
      .catch((error: any) => Observable.throw(error.json() || 'Server error'));
  }
}