import { Http } from '@angular/http';
import { RequestOptions } from '@angular/http';
import { Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class VersaoService {

  private URL_VERSAO = environment.urlService + '/versao';

  constructor(private http: Http) { }


  getOptions(): RequestOptions {

    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });

    return options;
  }


  getVersao(): Observable<String> {

    return this.http.get(this.URL_VERSAO + '/numero', this.getOptions()).map(res => res.text());
    
  }

}