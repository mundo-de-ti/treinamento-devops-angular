
import { ListagemComponent } from './view/estudante/listagem/listagem.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { CadastrarComponent } from './view/estudante/cadastrar/cadastrar.component';
import { DetalhesComponent } from './view/estudante/detalhes/detalhes.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'estudantes', pathMatch: 'full' },
    { path: 'estudantes', component: ListagemComponent },
    { path: 'estudantes/novo', component: CadastrarComponent },
    { path: 'estudantes/:id', component: DetalhesComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, {useHash: true})
    ],
    exports: [
        RouterModule
    ],
    providers: []
})
export class AppRoutingModule { }