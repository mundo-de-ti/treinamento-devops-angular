import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Estudante } from './../../../model/estudante';
import { EstudanteService } from './../../../service/estudante.service';

@Component({
  selector: 'app-listagem',
  templateUrl: './listagem.component.html',
  styleUrls: ['./listagem.component.css'],
  providers: [EstudanteService]
})
export class ListagemComponent implements OnInit {
  estudantees: Estudante[] = [];
   
  constructor(private service1: EstudanteService, private router: Router) { }

  ngOnInit() {

    this.service1.getEstudantees().subscribe(estudantees => this.estudantees = estudantees);

    //Outra forma de chamar o ws.
    //
    // this.service.getMedicos().subscribe(function (medicos) {
    //   this.medicos = medicos;
    // });
  }

  viewEstudante(estudante: Estudante) {
    this.router.navigate(['/estudantes/', estudante.id]);
  }



}
