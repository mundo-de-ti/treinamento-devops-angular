import { Route, Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Estudante } from './../../../model/estudante';
import { EstudanteService } from './../../../service/estudante.service';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.component.html',
  styleUrls: ['./detalhes.component.css'],
  providers: [EstudanteService]
})
export class DetalhesComponent implements OnInit {
  estudante: Estudante;
  retorno = "";

  constructor(
    private service: EstudanteService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.loadEstudante();
  }

  deleteEstudante(estudante: Estudante) {
    this.service.excluirEstudante(estudante.id).subscribe(retorno => {
      setTimeout(() => {
        this.router.navigate(['/estudantes']);
      }, 1000);
    });
  }

  private loadEstudante() {
    this.estudante = new Estudante();

    this.activatedRoute.params.subscribe(params => {
      const id = params['id'];
      if (id) { this.service.getEstudante(id).subscribe(estudante => this.estudante = estudante); }
    });
  }

}
