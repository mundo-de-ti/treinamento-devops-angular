import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Estudante } from './../../../model/estudante';
import { EstudanteService } from './../../../service/estudante.service';

@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.component.html',
  styleUrls: ['./cadastrar.component.css'],
  providers: [EstudanteService]
})
export class CadastrarComponent implements OnInit {

  estudante: Estudante = new Estudante();

  constructor(
    private estudanteService: EstudanteService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  save() {
    this.estudanteService.save(this.estudante).subscribe(estudante => {
      setTimeout(() => {
        this.router.navigate(['/estudantes']);
      }, 1000);
    });
  }
}
