import { Component, OnInit } from '@angular/core';
import { VersaoService } from './service/versao.service';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [VersaoService]
})
export class AppComponent implements OnInit {
  versao_service: String;
  versao_web: String
 
  constructor(private service1: VersaoService) { }

  ngOnInit() {

    this.versao_web = environment.versaoSistema;
    this.service1.getVersao().subscribe(versao1 => this.versao_service = versao1);
  }
}
